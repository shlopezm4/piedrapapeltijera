 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rspbetio;

import java.util.HashSet;
import java.util.Scanner;
import objetos.Jugador;
import objetos.Methods;

/**
 *
 * @author betio_000
 */
public class RSPBetio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] movimientos = {"PIEDRA","PAPEL","TIJERA"};
        Jugador cpu = new Jugador("CPU");
        Jugador j = new Jugador("JUGADOR");
        Methods m = new Methods();
        Scanner entrada = new Scanner(System.in);
        
        char ch = ' ';
        String c;
        boolean valido, mas = true;
        String des = " ";
        int partidas = 0, pcpu = 0, pj = 0;
        String ganador = " ";
        
        while(mas)
        {
          System.out.println("Para terminar la simulación: X.");
          
          try {
              do {
                System.out.print("acción(J/V/X): ");
                System.out.println("\n[J] - Jugar \n[V] - Ver Punteo \n[X] - Salir");
                c = entrada.nextLine();
                ch = c.charAt(0); 
                ch = Character.toUpperCase(ch);
                valido = (c.length() == 1) &&
               (ch == 'J' || ch == 'V' || ch == 'X');
              } while(!valido);
              
              if(ch == 'J')
              {
                partidas = partidas + 1;
                des = " ";
                des = m.desCpu(m.choice(), movimientos);
                cpu.setMov(des);
                System.out.println("Cual es tu movimiento?");
                j.setMov(entrada.nextLine().toUpperCase());
                System.out.println("Movimiento de Jugador: "+j.getMov());
                System.out.println("Movimiento de CPU: "+cpu.getMov());
                System.out.println("----------------------------------");
                ganador = m.play(cpu.getMov() , j.getMov(), movimientos) ;
                System.out.println("GANADOR: "+ ganador);
                if (ganador == "CPU")
                {
                    pcpu = pcpu + 1;
                }
                if (ganador == "JUGADOR")
                {
                    pj = pj + 1;
                }
                
              }
              else if(ch == 'V')
              {
                  System.out.println("PARTIDAS TOTALES: "+partidas);
                  System.out.println("GANADAS CPU: "+pcpu);
                  System.out.println("GANADAS JUGADOR: "+pj);
              }
              mas = !(ch == 'X');
          } catch (Exception e){
              
          }
          
        }
        
        
     
        
        //Funcion que lea los anteriores resultados
        //Funcion que lea elección de cpu y de jugador y diga quien ganó
        //Escribir en archivo las veces que ha ganado cada uno.
        
        
        
    }
    
}
